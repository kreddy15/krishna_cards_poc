package com.utilities;
import java.io.File;
import org.openqa.selenium.support.ui.ExpectedConditions;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;

import com.locators.Objects;

public class CommonFunctions extends StaticVariables {
	public CommonFunctions() {
	
		ProjectDir = System.getProperty("user.dir");
		File file = new File(ProjectDir + "\\screenshots");
		boolean a = false;
		if (!file.exists()) {
			a = file.mkdir();
		}
		if (a)
			System.out.println("screenshots folder successfully created.");
		else
			System.out.println("screenshots folder already exists.");

		ScreenshotsPath = ProjectDir + "\\screenshots\\";

		File ccfc = new File(ProjectDir + "/CCFCResults");
		if (!ccfc.exists()) {
			ccfc.mkdir();
			System.out.println("getPath: " + ccfc.getPath());
		}
		CCFCResultsPath = ProjectDir + "\\CCFCResults\\";

		System.out.println("Current Project Physical Location: " + ProjectDir + "; \n CCFCResultsPath: " + CCFCResultsPath);
		System.out.println("For screenshots path: " + ScreenshotsPath);
		System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.Jdk14Logger");
	}

	/************** Return Web Element *********/
	public WebElement GetWebElementByID(String elementlocator) {
		WebElement element = driver.findElement(By.id(elementlocator));
		return element;
	}

	public WebElement GetWebElementName(String elementlocator) {
		WebElement element = driver.findElement(By.name(elementlocator));
		return element;
	}

	public WebElement GetWebElementXpath(String elementlocator) {
		WebElement element = driver.findElement(By.xpath(elementlocator));
		return element;
	}

	public WebElement GetWebElementCss(String elementlocator) {
		WebElement element = driver.findElement(By.cssSelector(elementlocator));
		return element;
	}

	public WebElement GetWebElementLinkText(String elementlocator) {
		WebElement element = driver.findElement(By.linkText(elementlocator));
		return element;
	}

	public WebElement GetWebElementPartialLinkText(String elementlocator) {
		WebElement element = driver.findElement(By.partialLinkText(elementlocator));
		return element;
	}

	// Clear the Editbox Text
	public void ClearTextByName(String elementlocator) {
		driver.findElement(By.name(elementlocator)).clear();
	}

	public void ClearTextByID(By  elementlocator) {
		driver.findElement(elementlocator).clear();
	}

	public void ClearTextByXpath(String elementlocator) {
		driver.findElement(By.xpath(elementlocator)).clear();
	}

	/* *************Click Event************ */
	public void ClickByID(String elementlocator) {
		driver.findElement(By.id(elementlocator)).click();
	}

	public void ClickByName(String elementlocator) {
		driver.findElement(By.name(elementlocator)).click();
	}public void ClickByXpath(By elementlocator) {
		driver.findElement(elementlocator).click();
	}
public void ClickByCss(String elementlocator) {
		driver.findElement(By.cssSelector(elementlocator)).click();
	}

	public void ClickByLinkText(String elementlocator) {
		driver.findElement(By.linkText(elementlocator)).click();
	}

	public void ClickByPartialLinkText(String elementlocator) {
		// driver.findElement(By.partialLinkText(elementlocator)).click();
		WebElement element = driver.findElement(By.partialLinkText(elementlocator));
		element.click();
	}

	//public void ClickUsingJavaScript(WebElement webelement) {
		//((JavascriptExecutor) driver).executeScript("arguments[0].click();", webelement);
	//}

	/* *************Enter Text************ */
	public void EnterTextByID(By String, String InputText) {
		driver.findElement(String).sendKeys(InputText);
	}

	public void EnterTextByName(String elementlocator, String InputText) {
		driver.findElement(By.name(elementlocator)).sendKeys(InputText);
	}

	public void EnterTextByXpath(String elementlocator, String InputText) {
		driver.findElement(By.xpath(elementlocator)).sendKeys(InputText);
	}

	public void EnterTextByCss(String elementlocator, String InputText) {
		driver.findElement(By.cssSelector(elementlocator)).sendKeys(InputText);
	}

	// public static WebElement TxtUserName(WebDriver pDriver){
	public WebElement WebElementAction(WebDriver Driver) {
		WebElement myReturnElement = null;
		try {
			// myReturnElement = Driver.findElement(By.xpath(EBRObjects.Xp_CarModelSelectionpart1));

		} catch (Exception e) {
			System.out.println("Exception in TxtUserName" + e.getMessage());
		}
		return myReturnElement;
	}

	

	public int RandomNo_origin() {
		int randomInt = 0;
		Random rg = new Random();
		for (int idx = 1; idx <= 9; ++idx) {// 129
			randomInt = rg.nextInt(9);
			if (randomInt == 0) {
				randomInt = 1;
			}
			System.out.println("Generated : " + randomInt);
		}
		return randomInt;
	}
	
	

	
	public void SetDropDownByTestByXpath(String xPatchText, String disPlayText) throws Exception {

		List<WebElement> allOptions = driver.findElements(By.xpath(xPatchText));

		// System.out.println(" Elements count" +allOptions.size());

		for (WebElement webElement : allOptions) {

			if (webElement.getText().equals(disPlayText)) {

				System.out.println(webElement.getText());
				webElement.click();
				// Thread.sleep(1000);

			}

		}

	}
	

	public void switchToDefaultFrame() {
		driver.switchTo().defaultContent();
	}

	public void switchToFrame() {
		driver.switchTo().defaultContent();
		WebElement iFramesElement = driver.findElement(By.xpath("//iframe"));
		driver.switchTo().frame(iFramesElement);
	}

	public void switchToFrameByInt(int i) {
		driver.switchTo().defaultContent();
		driver.switchTo().frame(i);
	}

	public void scrollDown(int down) {
		System.out.println("***scrollDown: Move to default Content explicitly. Otherwise it won't work for Firefox***");
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("window.scrollBy(0," + down + ")", "");
	}

	public void scrollUp(int up) {
		System.out.println("***scrollUp: Move to default Content explicitly. Otherwise it won't work for Firefox***");
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("window.scrollBy(" + up + ",0)", "");
	}

	public String TimeStampasString() {
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
		return timeStamp;
	}

	public String TimeStampSQLasString() {
		java.util.Date date = new java.util.Date();
		String timeStamp = new Timestamp(date.getTime()).toString();
		return timeStamp;
	}

	public String TimeStamp2asStringwithAMPM() {
		String timeStamp = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a").format(Calendar.getInstance().getTime());
		return timeStamp;
	}

	public void takeScreenshot(String Name) throws IOException, Exception {
		// String filepath = FileOrDriverPathOf("ScreenshotsPath");

		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(scrFile, new File(ScreenshotsPath + Name + TimeStampasString() + ".png"));
			System.out.println("***Placed screen shot in " + ScreenshotsPath + " ***");
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public int RandomNo() {
		int randomInt = 0;
		Random rg = new Random();
		for (int idx = 1; idx <= 8; ++idx) {
			randomInt = rg.nextInt(8);
			if (randomInt == 0) {
				randomInt = 1;
			}
			System.out.println("Generated : " + randomInt);
		}
		return randomInt;
	}

	public int RandomNoforFleetSize() {
		int randomInt = 0;
		Random rg = new Random();
		for (int idx = 1; idx <= 3; ++idx) {
			randomInt = rg.nextInt(3);
			if (randomInt == 0) {
				randomInt = 1;
			}
			System.out.println("Generated : " + randomInt);
		}
		return randomInt;
	}

	public int IframeCount() {
		driver.switchTo().defaultContent();
		JavascriptExecutor exe = (JavascriptExecutor) driver;
		Integer numberOfFrames = Integer.parseInt(exe.executeScript("return window.length").toString());
		System.out.println("Number of iframes on the page are: " + numberOfFrames);

		// or

		// List<WebElement> iframeElements =
		// driver.findElements(By.tagName("iframe"));
		// System.out.println("The total number of iframes are " +
		// iframeElements.size());

		return numberOfFrames;
	}

	public void FrameNamesByTagName() {

		System.out.println("***FrameNamesByTagName: Move to default Content explicitly. Otherwise it won't work. 'Call switchToDefaultFrame and don't forget to move to your frame.' ***");
		System.out.println("Iframe Count: " + IframeCount());

		final List<WebElement> iframes = driver.findElements(By.tagName("iframe"));
		for (@SuppressWarnings("unused") WebElement iframe : iframes) {
			System.out.println("Iframes: " + iframes.toString());
		}
		System.out.println("***FrameNamesByTagName: Don't forget to switch to your frame. 'Call switchToDefaultFrame and don't forget to move to your frame.' ***");
	}

	public int getRandomNumberInRange(int min, int max) {

		if (min >= max) {
			// throw new IllegalArgumentException("max must be greater than min");
			return min;
		}
		Random r = new Random();
		return (r.nextInt((max - min) + 1) + min);
	}

	public void GetAndOpenBrowser(String TestDataPath) throws IOException {
		File file = new File(TestDataPath);
		FileInputStream fileInput = null;
		try {
			fileInput = new FileInputStream(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		Properties prop = new Properties();
		// load properties file
		try {
			prop.load(fileInput);
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (prop.getProperty("Browser").equalsIgnoreCase("IE")) {
			// IE browser script
			// System.setProperty("webdriver.ie.driver", StaticVariables.IEDriverpath);
			// System.setProperty("webdriver.ie.driver", FileOrDriverPathOf("IEDriverpath"));
			System.setProperty("webdriver.ie.driver", IEDriverPath());

			driver = new InternetExplorerDriver();
		}
		if (prop.getProperty("Browser").equalsIgnoreCase("CHROME")) {
			// Chrome browser script
			// System.setProperty("webdriver.chrome.driver", StaticVariables.ChromeDriverpath);
			// System.setProperty("webdriver.chrome.driver", FileOrDriverPathOf("ChromeDriverpath"));

			// DesiredCapabilities capability = DesiredCapabilities.chrome();
			// capability.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			System.setProperty("webdriver.ie.driver", ChromeDriverPath());
			driver = new ChromeDriver();
		}
		if (prop.getProperty("Browser").equalsIgnoreCase("FIREFOX")) {
			// Firefox browser script
			driver = new FirefoxDriver();
		}
		if (prop.getProperty("Browser").equalsIgnoreCase("Safari")) {
			// SafariDriver browser script
			driver = new SafariDriver();
		}
		driver.manage().deleteAllCookies();
	}

	public void GetAndOpenBrowserFromXML(String browser) throws IOException, InterruptedException {
			if (browser.equalsIgnoreCase("Firefox")) {
				System.setProperty("webdriver.gecko.driver", "C:/Users/kreddy/workspace/Notifications_automation/drivers/geckodriver.exe");
				driver = new FirefoxDriver();
			} else if (browser.equalsIgnoreCase("chrome")) {
				// System.setProperty("webdriver.chrome.driver", FileOrDriverPathOf("ChromeDriverpath"));
				System.setProperty("webdriver.chrome.driver", ChromeDriverPath());
				driver = new ChromeDriver();
			} else if (browser.equalsIgnoreCase("IE")) {
				// System.setProperty("webdriver.ie.driver", FileOrDriverPathOf("IEDriverpath"));
				System.setProperty("webdriver.ie.driver", IEDriverPath());
				DesiredCapabilities capabilities = new DesiredCapabilities();
				capabilities = DesiredCapabilities.internetExplorer();
				capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
				// DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
				driver = new InternetExplorerDriver(capabilities);
			} else if (browser.equalsIgnoreCase("Safari")) {
				driver = new SafariDriver();
			} 
	}
		

	/*
	 * public String FileOrDriverPathOf(String GetValueFor) throws IOException { Properties prop = new Properties(); String output = ""; InputStream in =
	 * getClass().getResourceAsStream(FilePathAndDriverPathPropertyFileName); if (in != null) { prop.load(in); output = prop.getProperty(GetValueFor); System.out.println("prop.getProperty(" +
	 * GetValueFor + "): " + output); in.close(); } else { throw new FileNotFoundException("property file '" + FilePathAndDriverPathPropertyFileName + "' not found in the classpath"); } return output;
	 * }
	 */

	public String TestDataPathOf(String TestDataFileName) throws IOException {
		String TestDataPath = ProjectDir + "\\Testdata\\" + TestDataFileName;
		return TestDataPath;
	}

	public String IEDriverPath() throws IOException {
		String IEDriverPath = ProjectDir + "\\drivers\\" + "IEDriverServer.exe";
		return IEDriverPath;
	}

	public String ChromeDriverPath() throws IOException {
		String ChromeDriverPath = ProjectDir + "\\drivers\\" + "chromedriver.exe";
		return ChromeDriverPath;
	}

	public void ScreenshotOnPassFail(ITestResult testResult) throws IOException {
		StaticVariables.ClassName = testResult.getTestClass().getName().trim();
		StaticVariables.MethodName = testResult.getName().trim();
		String ClsNmMtdNm = StaticVariables.ClassName + "_" + StaticVariables.MethodName + "_";
		// String path = FileOrDriverPathOf("ScreenshotsPath");
		// System.out.println("StaticVariables.ClassName: " +
		// StaticVariables.ClassName);
		// System.out.println("StaticVariables.MethodName: " +
		// StaticVariables.MethodName);

		if (testResult.getStatus() != ITestResult.FAILURE) {
			File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			System.out.println("ClsNmMtdNm: " + ClsNmMtdNm + "; TimeStampasString" + TimeStampasString());
			FileUtils.copyFile(scrFile, new File(ScreenshotsPath + "Pass_" + ClsNmMtdNm + TimeStampasString() + ".jpg"));
		}
		if (testResult.getStatus() == ITestResult.FAILURE) {
			File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			System.out.println("ClsNmMtdNm: " + ClsNmMtdNm + "; TimeStampasString" + TimeStampasString());
			FileUtils.copyFile(scrFile, new File(ScreenshotsPath + "Fail_" + ClsNmMtdNm + TimeStampasString() + ".jpg"));
		}
	}

	public void ScrollToElement(WebElement element) {
		System.out.println("***ScrollToElement: ***");// ((JavascriptExecutor)
		// driver).executeScript("arguments[0].scrollIntoView(true);", element);
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);
		((JavascriptExecutor) driver).executeScript("arguments[0].style.border='6px groove green'", element);
	}

	public void ScrollToElementBottom(WebElement element) {
		System.out.println("***ScrollToElementBottom:  ***");
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(false);", element);
		((JavascriptExecutor) driver).executeScript("arguments[0].style.border='6px groove green'", element);
	}

	public void HighlightElement(WebElement element) throws InterruptedException {
		((JavascriptExecutor) driver).executeScript("arguments[0].style.border='6px groove green'", element);
		Thread.sleep(1000);
		((JavascriptExecutor) driver).executeScript("arguments[0].style.border=''", element);
	}

	public void ScrollToXY(int x, int y) {
		System.out.println("***ScrollToXY: Move to default Content explicitly. Otherwise it won't work. 'Call switchToDefaultFrame and don't forget to move to your frame.' ***");
		String script = "window.scrollTo(" + x + "," + y + ");";
		((JavascriptExecutor) driver).executeScript(script);
	}

	public int GetXLocationOfElement(WebElement element) {
		System.out.println("***GetXLocationOfElement: Don't forget to move to your frame.***");
		Point point = element.getLocation();
		int x = point.getX();
		System.out.println("X Coordination of the Element: " + x);
		return x;
	}

	public int GetYLocationOfElement(WebElement element) {
		System.out.println("***GetYLocationOfElement: Don't forget to move to your frame.***");
		Point point = element.getLocation();
		int y = point.getY();
		System.out.println("Y Coordination of the Element: " + y);
		return y;
	}

	public void ScrollToBottom() {
		System.out.println("***ScrollToBottom: Move to default Content explicitly. Otherwise it may not work. 'Call switchToDefaultFrame and don't forget to move to your frame.' ***");

		System.out.println("ScrollToBottom: document.body.scrollHeight: " + ((JavascriptExecutor) driver).executeScript("return document.body.scrollHeight;"));
		((JavascriptExecutor) driver).executeScript("window.scrollTo(0,document.body.scrollHeight);");
	}

	public void ScrollToTop() {
		System.out.println("***ScrollToTop: Move to default Content explicitly. Otherwise it may not work. 'Call switchToDefaultFrame and don't forget to move to your frame.' ***");

		((JavascriptExecutor) driver).executeScript("window.scrollTo(0,0);");
	}

	/*
	 * public void Fluent_Wait() {
	 * 
	 * Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(30, TimeUnit.SECONDS).pollingEvery(5, TimeUnit.SECONDS).ignoring(NoSuchElementException.class);
	 * 
	 * WebElement foo = wait.until(new Function<WebDriver, WebElement>() {
	 * 
	 * public WebElement apply(WebDriver driver) { return driver.findElement(By.id("foo")); } });}
	 */

	/*public void Fluent_Wait(final WebElement El) {

		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(30, TimeUnit.SECONDS).pollingEvery(5, TimeUnit.SECONDS).ignoring(NoSuchElementException.class);

		WebElement foo = wait.until(new Function<WebDriver, WebElement>() {

			public WebElement apply(WebDriver driver) {

				return El;
			}
		});
	}*/

	// public void FluentWait() {
	// Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(30, TimeUnit.SECONDS).pollingEvery(5,
	// TimeUnit.SECONDS).ignoring(NoSuchElementException.class).ignoring(StaleElementReferenceException.class);
	// wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("body form#webDriverUnitiFrame1TestFormID h1")));
	//
	// // http://www.seleniumeasy.com/selenium-tutorials/webdriver-wait-examples
	// }

/*	public WebElement fluentWait(final By locator) {
		// http://toolsqa.com/selenium-webdriver/advance-webdriver-waits/
		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(180, TimeUnit.SECONDS).pollingEvery(1, TimeUnit.SECONDS).ignoring(NoSuchElementException.class);
		WebElement elwait = wait.until(new Function<WebDriver, WebElement>() {
			public WebElement apply(WebDriver driver) {
				return driver.findElement(locator);
			}
		});
		return elwait;
	}*/


	
	/*public static ElementPresent(locator){
		Objects obj= new Objects();
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element =wait.until(ExpectedConditions.elementToBeClickable(locator));
		return locator;
	}*/
	
	
	public int WaitforElementPresent(final By locator) throws InterruptedException{
		int elementpresentcount=0;
		while(elementpresentcount<1){
			elementpresentcount = driver.findElements(locator).size();
			Thread.sleep(1000);
		}
		return elementpresentcount;
	}
	
	public String RemoveSpaceAndNonAlphaFromString(String inputString) {
		String Output = inputString.replaceAll("[^a-zA-Z]", "");
		return Output;
	}

	public String RemoveNonAlphaExcludingSpaceFromString(String inputString) {
		String Output = inputString.replaceAll("[^a-z A-Z]", "");
		return Output;
	}

	public String RemoveNonAlphaNumericFromString(String inputString) {
		String Output = inputString.replaceAll("[^a-z A-Z0-9]", "");
		return Output;
	}public String RemoveNonNumericFromString(String inputString) {
		String Output = inputString.replaceAll("[^0-9]", "");
		return Output;
	}

	public String RemoveSpaceAndAlpha(String inputString) {
		String Output = inputString.replaceAll("[a-z A-Z]", "");
		return Output;
	}

	public void CheckBoxSelectedOrNot_getElementByID(String LocatorByID) {
		String Script = "return document.getElementById('" + LocatorByID + "').checked;";
		System.out.println("JScript: " + ((JavascriptExecutor) driver).executeScript(Script));
	}

	public void assertEquals(String receivedMessage, String expectedMessage) throws Exception {
		Thread.sleep(2000);
		Assert.assertEquals(receivedMessage, expectedMessage);
	}

	// For OptOut Result

	

	
	/*public int getResponseCode(String url) {

		// https://rogerkeays.com/how-to-get-the-http-status-code-in-selenium-webdriver
		// https://www.seleniumeasy.com/selenium-tutorials/how-to-find-broken-links-using-webdriver-java
		try {
			return Request.Get(url).execute().returnResponse().getStatusLine().getStatusCode();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}*/

	// Type 2
	/*public void verifyURLStatus(String URL) {
		// http://roadtoautomation.blogspot.in/2013/04/road-to-verify-200-response-code-of-web.html
		try {
			WebClient webClient = new WebClient();
			HtmlPage htmlPage = webClient.getPage(URL);
			System.out.println("Status Code: " + htmlPage.getWebResponse().getStatusCode() + "; Web Response: " + htmlPage.getWebResponse().getStatusMessage());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/

	public int loopAllframesAndReturnCountofElement(final By locator) throws InterruptedException {
		// WebElement found = fluentWait(By.xpath(xPathElement));
		int ElementpresenceCount = 0;
		IframeCount();

		/*
		 * while (ElementpresenceCount < 1) { try { Thread.sleep(250); switchToFrameByInt(Loop); ElementpresenceCount = driver.findElements(locator).size();
		 * System.out.println("Try LoopAllframesAndReturnWebEL: " + Loop + "; ElementpresenceCount: " + ElementpresenceCount); Loop++; if (ElementpresenceCount > 0 || Loop > maxFramaecount) { break; }
		 * } catch (Exception ex) { System.out.println("Catch LoopAllframesAndReturnWebEL: " + Loop + "; " + ex); }
		 */
		while (ElementpresenceCount < 1) {
			ElementpresenceCount = driver.findElements(locator).size();
			Thread.sleep(250);
		}
		return ElementpresenceCount;
	}

	public int loopAllframesAndReturnCountofElementOld(final By locator) {
		// WebElement found = fluentWait(By.xpath(xPathElement));
		int ElementpresenceCount = 0;
		int Loop = 0;
		int maxFramaecount = IframeCount();

		while (ElementpresenceCount < 1) {
			try {
				Thread.sleep(250);
				switchToFrameByInt(Loop);
				ElementpresenceCount = driver.findElements(locator).size();
				System.out.println("Try LoopAllframesAndReturnWebEL Old: " + Loop + "; ElementpresenceCount: " + ElementpresenceCount);
				Loop++;
				if (ElementpresenceCount > 0 || Loop > maxFramaecount) {
					break;
				}
			} catch (Exception ex) {
				System.out.println("Catch LoopAllframesAndReturnWebEL Old: " + Loop + "; " + ex);
			}
		}
		return ElementpresenceCount;
	}

	public WebElement ReturnWebElementByAnyLocator(final By locator) {
		WebElement RetEl = driver.findElement(locator);
		return RetEl;
	}

	public void SafeClick(final By locator) throws InterruptedException {
		WebElement wesafe = driver.findElement(locator);
		Boolean flag = false;
		int loop = 0;
		while (flag == false) {
			try {
				Thread.sleep(500);
				wesafe.click();
				Thread.sleep(2000);
				flag = true;
				System.out.println("Try: SafeClick: " + loop);
				loop++;
				if (loop > 12) {
					break;
				}
			} catch (Exception ex) {
				flag = false;
				Thread.sleep(1000);
				System.out.println("Catch: SafeClick: " + loop);
			}

		}
	}

}
	
	


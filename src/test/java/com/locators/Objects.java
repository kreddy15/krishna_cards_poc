package com.locators;

import org.openqa.selenium.By;

public class Objects {

	
	public final By userid = By.id("md-input-1");
	public final By passwd = By.id("md-input-3");
	public final By loginbutton = By.xpath("/html/body/app-root/app-login/div/div[2]/div[2]/app-login-form/md-card/form/div/button/div[1]");
	public final By navigatespending = By.xpath("/html/body/app-root/app-dashboard/div/app-three-column-left-nav-layout/section/article[2]/div/app-chapter-navigation/nav/md-list/md-list-item[3]/div/a");
	public final By showmore = By.xpath("/html/body/app-root/app-dashboard/div/app-three-column-left-nav-layout/section/article[3]/div/app-chapter-landing/div/cta-button/a");
}
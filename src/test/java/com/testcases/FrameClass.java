package com.testcases;



import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.utilities.CommonFunctions;



public class FrameClass extends com.utilities.StaticVariables {
	public String SurName, titleName, carName;
	public String TestDataPath = "";
	public String BaseURL, intmodelNo;
	int randomInt;

	CommonFunctions cfn = new CommonFunctions();

	@BeforeClass
	@Parameters("browser")
	public void beforeclass(@Optional("ie") String browser) throws IOException, InterruptedException {
		TestDataPath = cfn.TestDataPathOf("input.properties");
		Browser = browser;
		cfn.GetAndOpenBrowserFromXML(Browser);
		System.out.println("ScreenshotsPath: " + ScreenshotsPath);
	}

	@AfterClass
	public void End() throws IOException {
		driver.quit();
	}
	@AfterMethod
	public void StatusScreenShots(ITestResult testResult) throws IOException, InterruptedException {
		cfn.ScreenshotOnPassFail(testResult);
		Thread.sleep(1000);
	}
	
	
@Test
	public void TD_Positive() throws Exception {

		File file = new File(TestDataPath);
		Properties prop = new Properties();

		FileInputStream fileInput = null;
		try {
			fileInput = new FileInputStream(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		// load properties file
		try {
			prop.load(fileInput);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}


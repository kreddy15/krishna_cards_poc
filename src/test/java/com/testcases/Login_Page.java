package com.testcases;

import java.io.File;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.locators.Objects;
import com.utilities.CommonFunctions;
import com.utilities.StaticVariables;

import junit.framework.Assert;

public class Login_Page extends StaticVariables {
	public String SurName, titleName, carName;
	public String TestDataPath = "";
	public String BaseURL, intmodelNo;
	int randomInt;

	CommonFunctions cfn = new CommonFunctions();
	Objects obj= new Objects();
	@BeforeClass
	@Parameters("browser")
	public void beforeclass(@Optional("chrome") String browser) throws IOException, InterruptedException {
		TestDataPath = cfn.TestDataPathOf("input.properties");
		Browser = browser;
		cfn.GetAndOpenBrowserFromXML(Browser);
		System.out.println("ScreenshotsPath: " + ScreenshotsPath);
	}

	@AfterClass
	public void End() throws IOException {
		driver.quit();
	}

	@AfterMethod
	public void StatusScreenShots(ITestResult testResult) throws IOException, InterruptedException {
		cfn.ScreenshotOnPassFail(testResult);
		Thread.sleep(1000);
	}

	@Test
	public void Login_Together() throws Exception {

		File file = new File(TestDataPath);
		Properties prop = new Properties();

		FileInputStream fileInput = null;
		try {
			fileInput = new FileInputStream(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		// load properties file
		try {
			prop.load(fileInput);
		} catch (IOException e) {
			e.printStackTrace();
		}
		driver.get(prop.getProperty("url"));
		driver.manage().window().maximize();
		cfn.WaitforElementPresent(obj.userid);
		cfn.ClearTextByID(obj.userid);
		cfn.EnterTextByID(obj.userid, prop.getProperty("username"));
		cfn.ClearTextByID(obj.passwd);
		cfn.EnterTextByID(obj.passwd, prop.getProperty("password"));
		cfn.WaitforElementPresent(obj.loginbutton);
		cfn.ClickByXpath(obj.loginbutton);
		cfn.WaitforElementPresent(obj.navigatespending);
		assert driver.getCurrentUrl().contains("dev.brightpeakfinancial.com/dashboard");
		cfn.ClickByXpath(obj.navigatespending);
		Thread.sleep(2000);
		validateCards();
			
	}
	public void validateCards(){
		if(driver.findElement(obj.showmore).isDisplayed()){cfn.ClickByXpath(obj.showmore);}
			assert driver.getPageSource().contains("Credit card debt");	
			assert driver.getPageSource().contains("Disability insurance");	
			assert driver.getPageSource().contains("Has student loans");	
			assert driver.getPageSource().contains("Credit card debt");
			assert driver.getPageSource().contains("Disability insurance");
			assert driver.getPageSource().contains("Has kids");
			assert driver.getPageSource().contains("Big purchase savings");
			assert driver.getPageSource().contains("Health insurance");
			assert driver.getPageSource().contains("Life insurance");
			assert driver.getPageSource().contains("Emergency Savings");
			assert driver.getPageSource().contains("Retirement Savings");	
		}
}
